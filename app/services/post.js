const { Post } = require("../models");

module.exports = {
  getAllPosts() {
    const posts = Post.findAll();
    return posts;
  },
  createPost(input) {
    const post = Post.create(input);
    return post;
  },
  updatePost(selectPost, newPost) {
    return selectPost.update(newPost);
  },

  setPost(selectPost) {
    return Post.findByPk(selectPost);
  },

  destroyPost(selectPost) {
    return selectPost.destroy();
  },

};
